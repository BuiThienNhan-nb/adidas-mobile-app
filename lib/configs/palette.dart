import 'package:flutter/material.dart';

class AppColors {
  static const backgroundColor = Color(0xffFFFFFF);
  static const iconBackgroundColor = Color(0xff000000);
  static const buttonOnClick = Color(0xFF9E9E9E);
  static const greyBackground = Color(0xFFBDBDBD);
  static const extraGreyBackground = Color(0xFFE0E0E0);
  static const sneakerItemBackground = Color(0xFFeceff1);
  static const itemTagBackground = Color(0xFFd89610);

  static const subTitleText = Color(0xFF9E9E9E);

  static const navyFont = Color(0xFF344f99);
}
